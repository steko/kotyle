#-*- coding:utf-8 -*-
"""
Kotyle
~~~~~~

A Python package to measure the capacity of a ceramic vessel from
its profile.

:copyright: (c) 2020 by Stefano Costa <steko@iosa.it>
:license: Apache License 2.0
"""

import math


class Point(object):
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __repr__(self):
        return "Point(x=%s, y=%s)" % (self.x, self.y)


class LineString(object):
    '''Builds a Line object from an iterable of coordinate pairs.'''

    def __init__(self, points):
        self.coords = [ [p[0], p[1]] for p in points ]

    @property
    def length(self):
        return self._linestring_length()

    @property
    def centroid(self):
        sum_x, sum_y, total_length = (0, 0, 0)
        for n, xy in enumerate(self.coords):
            x0, y0 = xy
            try:
                x1, y1 = self.coords[n+1]
            except IndexError:
                continue
            else:
                segment_len = math.sqrt((x1-x0)**2 + (y1-y0)**2)
                line_center_x = (x0 + x1) / 2
                line_center_y = (y0 + y1) / 2
                sum_x += segment_len * line_center_x
                sum_y += segment_len * line_center_y
                total_length += segment_len
        cx = sum_x / total_length
        cy = sum_y / total_length
        return Point(cx, cy)

    def _linestring_length(self):
        cumdist = 0
        for n, xy in enumerate(self.coords):
            x0, y0 = xy
            try:
                x1, y1 = self.coords[n+1]
            except IndexError:
                continue
            else:
                dist = math.sqrt((x1-x0)**2 + (y1-y0)**2)
                cumdist += dist
        return cumdist


class Polygon(object):
    '''Builds a Polygon object from an iterable of coordinate pairs.'''

    def __init__(self, points):
        self.coords = [(p[0], p[1]) for p in points ]

    @property
    def area(self):
        return self._shoelace_area()

    @property
    def centroid(self):
        return self._area_centroid()

    def _shoelace_area(self):
        '''The area of a polygon with Gauss's or shoelace formula.'''

        xs, ys = zip(*self.coords)

        yl = ys[1:] + ys[:1] # make 1st item last
        temp1 = sum(xy[0]*xy[1] for xy in zip(xs, yl))

        xl = xs[1:] + xs[:1] # make 1st item last
        temp2 = sum(xy[0]*xy[1] for xy in zip(xl, ys))

        return abs(temp1 - temp2) / 2.0

    def _area_centroid(self):
        '''The centroid of a polygon.'''

        xs, ys = zip(*self.coords)

        yl = ys[1:] + ys[:1] # make 1st item last
        xl = xs[1:] + xs[:1] # make 1st item last

        cx = 0
        for n, x in enumerate(xs):
            cx += (x + xl[n]) * (x * yl[n] - xl[n] * ys[n])
        cx = cx / (self.area * 6.0)

        cy = 0
        for n, y in enumerate(ys):
            cy += (y + yl[n]) * (y * xl[n] - yl[n] * xs[n])
        cy = cy / (self.area * 6.0)

        return Point(cx, cy)


def point_to_line_distance(point, line):
    a, b = line.coords
    p = point
    normal_length = math.sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y))
    dist =  abs((p.x - a.x) * (b.y - a.y) - (p.y - a.y) * (b.x - a.x)) / normal_length
    return dist

def mirror(geom, axis):
    '''Reflects a geometry using a line as simmetry axis.'''

    pass
