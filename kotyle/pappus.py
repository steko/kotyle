#-*- coding:utf-8 -*-
"""
Kotyle
~~~~~~

A Python package to measure the capacity of a ceramic vessel from its
profile.

This module performs calculations based on Pappus's theorems.

:copyright: (c) 2020 Stefano Costa <steko@iosa.it>
:license: Apache License 2.0
"""

import math

from . import LineString, Polygon

class VesselCapacity(object):
    '''Compute the capacity of a ceramic vessel from its geometric
    profile.'''

    def __init__(self, geom_profile):
        xs, ys = zip(*geom_profile["geometry"]["coordinates"])

        # workaround mirrored profile
        xorder = xs[0] > xs[-1]
        yorder = ys[0] > ys[-1]

        # reduce coordinates to standard origin
        xmin = min(xs)
        ymin = min(ys)
        xmax = max(xs)

        if xorder and not yorder:
            xs = [xmax - x for x in xs]
        elif yorder and not xorder:
            xs = [xmax - x for x in xs]
        else:
            xs = [x - xmin for x in xs]
        ys = [y - ymin for y in ys]

        coords = list(zip(xs,ys))

        self.linestring = LineString(coords)
        self.arc_length = self.linestring.length
        self.s_centroid = self.linestring.centroid

        ymax = max(ys) # find missing corner
        coords.append((0.0, ymax))

        self.polygon = Polygon(coords)
        self.area = self.polygon.area
        self.v_centroid = self.polygon.centroid

    def surface(self):
        '''The curve surface of the vessel.'''

        s_distance = 2 * math.pi * self.s_centroid.x * self.arc_length

        # Pappus's theorem
        return self.arc_length * s_distance

    def volume(self):
        '''The capacity of the vessel.'''

        v_distance = 2 * math.pi * self.v_centroid.x

        # Pappus's theorem
        return self.area * v_distance

    def __str__(self):
        surface = 'Surface: %5.7f m²' % self.surface()
        volume = 'Capacity: %5.7f ℓ' % (self.volume())
        return '%s, %s' % (surface, volume)
