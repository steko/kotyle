#-*- coding:utf-8 -*-

import json
import unittest

import os

from kotyle import pappus

class TestPappusSurfaceAndVolume(unittest.TestCase):

    def setUp(self):
        this_dir = os.path.dirname(__file__)
        with open(os.path.join(this_dir,"test_profile.json")) as f:
            data = json.load(f)
            self.vessel = pappus.VesselCapacity(data)

    def test_surface(self):
        self.assertAlmostEqual(self.vessel.surface(), 0.0130521)

    def test_volume(self):
        self.assertAlmostEqual(self.vessel.volume(), 0.0013724)

    def test_str(self):
        self.assertEqual(str(self.vessel),"Surface: 0.0130521 m², Capacity: 0.0013724 ℓ")

if __name__ == '__main__':
    unittest.main()
