#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Kotyle
~~~~~~

A Python package to measure the capacity of a ceramic vessel from its
profile.

This module loads an existing JSON file with measurement data and
calculates the vessel capacity.

:copyright: (c) 2020 Stefano Costa <steko@iosa.it>
:license: Apache License 2.0
"""

import argparse, json

from kotyle.pappus import VesselCapacity

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('profile', metavar='PROFILE', type=file, nargs='+',
                   help='the file(s) to process')

args = parser.parse_args()

def metro(file):
    profile = json.loads(file.read())
    vessel = VesselCapacity(profile)
    d =  {'volume': vessel.volume()*1000,
          'title':  profile['properties']['title'],
          }
    return d

def main():
    for f in args.profile:
        d = metro(f)
        print('The capacity of "{title}" is {volume:03.2f} ℓ'.format(**d))

if __name__ == '__main__':
    main()
