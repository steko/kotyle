.. Kotyle documentation master file, created by
   sphinx-quickstart on Sun Feb 20 01:30:03 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====================================
 Kotyle --- measures vessel capacity
=====================================

Kotyle (from the ancient greek κοτύλη, “measure of capacity”,
“drinking cup”) is a software program for calculating the capacity of
a ceramic vessel. The main use of Kotyle is for artifacts studied by
archaeologists.

Kotyle is written in the Python programming language and is available
under the Apache Software License 2.0.

Kotyle can be downloaded from the `Git repository`_ at Codeberg.

For a quick start, look at the :ref:`gimp-plugin` and :ref:`metro` pages.

.. _`Git repository`: https://codeberg.org/steko/kotyle

Contents
========

.. toctree::
   :maxdepth: 2

   intro
   format
   gimp-plugin
   metro

   volume
   surface
   weight_density
   other

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Logo: `Kotyle with satyr`_, from W. Lamb, *Seven vases from the Hope
collection*, in *Journal of Hellenic Studies* 38, 1918.

.. _`Kotyle with satyr`: http://books.google.com/books?id=KVPQAAAAMAAJ&hl=it&pg=PP69#v=onepage&q=kotyle&f=false

