.. _format:

========================================================
 Describing the profile of a vessel in a digital format
========================================================

The most difficult part of the story is finding an acceptable digital format
for describing the profile of a vessel.

For **drawing** purposes, it is common to use vector graphics software
or CAD systems. It is clear that it is highly desirable to represent
the profile as a vector geometry, that is, a sequence of points and
lines in a cartesian space.

However, all those programs are made for drawing, and not for
describing geometric entities in a purely **abstract** fashion. We
don't need line widths, colors and other such features. Furthermore,
we don't want to work in a coordinate system where the origin (the
``0,0`` point) is in the top left corner, like for example SVG_
does. We don't need anything that has to do with graphics, just a
compact way of representing a (tiny) set of ordered geometric
coordinates.

.. _SVG: http://www.w3.org/Graphics/SVG/

Making sense of different standards
===================================

To add more fun, profile drawings can be both right-handed or
left-handed. We could force people to mirror their images before
feeding them into Kotyle, but why would you do that ? Computers should
be working for us, not the opposite !

It's quite easy to manage this disparity, even taking into account
that people might start drawing from the rim (top) or from the bottom
of the vessel.

+---------+---------+---------+
|         | x0 > xN | x0 < xN |
+---------+---------+---------+
| y0 > yN |    ↙    |    ↘    |
+---------+---------+---------+
| y0 < yN |    ↖    |    ↗    |
+---------+---------+---------+

``x0`` is the ascissa of the first point in the sequence, and ``xN``
the ascissa of the last point. Same for ``y0`` and ``yN``. Arrows
indicate in which direction the profile was drawn. “Same sign”
combinations give the same result.

The standardized view is with the ``0,0`` point at the bottom of the
profile, where it crosses the rotation axis. But it's easy to
``reverse()`` those profiles that follow the opposite standard,
mirroring them.


CSV: a basic approach
=====================

Until now, the most convenient way for doing so may be a good old CSV
file::

    11.5989847716,11.4316005076
    12.4873096447,11.3938563452
    13.4263959391,11.387836802
    14.6700507614,11.4760149746
    16.1675126904,11.6587162437
    16.7512690355,11.7511243655
    17.9441624365,12.0639779188
    18.7563451777,12.6677218274
    19.3147208122,13.4012926396
    19.6446700508,14.008127665
    19.7715736041,14.1675642132
    19.7969543147,14.2635515228
    19.7969543147,14.3917515228
    19.923857868,14.8075880711

While it might seem awkward at first sight, it contains all the
information we need. The only problem with this approach is that there
is no place for metadata like the name and description of the
vessel.

GeoJSON: going further
======================

A slighly more advanced encoding of the same data can be
expressed as GeoJSON_:

.. code-block:: json

  { "type": "Feature",
    "geometry": {
      "type": "LineString",
        "coordinates": [
	      [11.5989847716,11.4316005076],
	      [12.4873096447,11.3938563452],
	      [13.4263959391,11.387836802],
	      [14.6700507614,11.4760149746],
	      [16.1675126904,11.6587162437],
	      [16.7512690355,11.7511243655],
	      [17.9441624365,12.0639779188],
	      [18.7563451777,12.6677218274],
	      [19.3147208122,13.4012926396],
	      [19.6446700508,14.008127665],
	      [19.7715736041,14.1675642132],
	      [19.7969543147,14.2635515228],
	      [19.7969543147,14.3917515228],
	      [19.923857868,14.8075880711]
        ]
      },
    "properties": {
      "name": "Hayes 61 B",
      "description": "A quite small version of 61 B.",
      "author": "Stefano Costa"
    }
  }


This format is cheap, and it can be read directly by dedicated
geometry libraries. It's not easy to type, but it's definitely easy to
read. Plus, the idea is that you *don't* have to type it, because a
dedicated program can do the work for you.

If you use the GIMP, Kotyle has :ref:`gimp-plugin` that export vector
paths to GeoJSON and facilitate creating digital profiles.

GeoJSON profiles can be analysed with :ref:`metro`.

.. _GeoJSON: http://geojson.org/
