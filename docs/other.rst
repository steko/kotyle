.. _other:

===============================================
 Other programs that calculate vessel capacity
===============================================

Inspiration for writing Kotyle derived from other existing tools, that
I didn't find satisfactory for my purposes.

VESCAP
======

VESCAP (VESsel CAPacity) is a standardized procedure based on AutoCAD
used together with the CAD Overlay plugin to digitize a raster
drawing.

The main advantage of the VESCAP routine lies in avoiding the
separation between the drawing of the potsherd and the geometric
definition of the vessel profile. Both can be stored in the same CAD
file, and the solid of revolution can be easily created. AutoCAD gives
directly the volume of the solid, so the results are guaranteed
correct.

VESCAP was published by James McCaw in 2007 [Peña07]_.

.. [Peña07]
   J. Theodore Peña, *The quantitative analysis of Roman
   pottery: general problems, the methods employed at the Palatine
   East, and the supply of African Sigillata to Rome*, in E. Papi
   (ed.) *Supplying Rome and the Empire*, pp. 153-172


ARCANE Pottery Utility
======================

Far from being a magic tool, the `Pottery Utility`_ developed by the
ARCANE Project is a dedicated stand-alone program devoted to
measurements of pottery drawings and profiles of vessels.

However, the method it uses to calculate volume has a major flaw,
because it assumes a *monotonic* profile, that is, a profile where the
lowest point lies on the rotation axis. There are some cases where
such a condition doesn't hold.

Pottery Utility is based on Shockwave Flash.

.. _`Pottery Utility`: http://www.metraweb.net/additionaltools.htm


Amphoralex
==========

There is a volume calculation program available from Amphoralex_, the
website of the *Centre Alexandrin d'Étude des Amphores*.

It appears to be based on FileMaker 5.

.. _Amphoralex: http://amphoralex.org/amphores/CalculVolume/CalculVolume.php


Calcul de capacité d'un récipient à partir de son profil
========================================================

This is a web service developed by CReA_
(Centre de Recherches en Archéologie et Patrimoine).

.. _CRea: http://lisaserver.ulb.ac.be/capacity/

Registration is needed.
