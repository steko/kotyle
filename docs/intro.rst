.. _intro:

======================================================
 Introduction, or why would you want to know capacity
======================================================

Measuring capacity of a ceramic vessel is not part of the ordinary
archaeological process that starts with the recovering of artifacts
during excavation and ends with publication as a drawing of the
profile. More recently, photographic pictures of sherds and vessels
are included in archaeological publications.

Transport vessels
=================

Capacity is of paramount importance for transport vessels like
amphorae. Should you fail to recognize this importance, you may end up
comparing numbers that have no meaning. If you have 3 *Africana
grande* and 3 *Keay 26* (*spatheion*), you're not dealing with the
same amount of 2 different types of vessels, but with a dramatically
different quantity of oil or wine.

Eating vessels
==============

Even for the study of tablewares, capacity can be a key factor to
distinguish between individual and collective vessels.
