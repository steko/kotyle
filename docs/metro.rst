.. _metro:

====================================
 Μετρω, the Kotyle measurement tool
====================================

This is a simple command-line Python script that reads data from a
GeoJSON file (see :ref:`format`) and outputs the vessel volume.

It is found in the source tree of Kotyle.

Usage
=====

Example usage:

.. code-block:: shell-session
   :emphasize-lines: 2

   $ python metro.py ~/profile.json
   The capacity of "GQB 266.231" is 6.04 ℓ

In this example, the input file ``profile.json`` has the following content:

.. code-block:: json

    {
        "geometry": {
            "type": "LineString",
            "coordinates": [
                [
                    0.13614400000000002,
                    0.20726400000000003
                ],
                [
                    0.13893799999999998,
                    0.19431000000000004
                ],
                [
                    0.138176,
                    0.18669000000000002
                ],
                [
                    0.133096,
                    0.1778
                ],
                [
                    0.120396,
                    0.165354
                ],
                [
                    0.107696,
                    0.15417800000000004
                ],
                [
                    0.10033,
                    0.13741400000000004
                ],
                [
                    0.096012,
                    0.11531600000000002
                ],
                [
                    0.09982200000000001,
                    0.09931400000000001
                ],
                [
                    0.10744200000000001,
                    0.07594600000000004
                ],
                [
                    0.11683999999999999,
                    0.05994400000000005
                ],
                [
                    0.124714,
                    0.043180000000000024
                ],
                [
                    0.135128,
                    0.02895600000000001
                ],
                [
                    0.1397,
                    0.02235200000000001
                ],
                [
                    0.158496,
                    0.014224000000000042
                ],
                [
                    0.17653,
                    0.005842000000000014
                ],
                [
                    0.19583399999999998,
                    0.0012700000000000489
                ],
                [
                    0.21488400000000002,
                    0.0
                ]
            ]
        },
        "type": "Feature",
        "properties": {
            "author": "Stefano Costa",
            "description": "Cooking pot",
            "title": "GQB 266.231"
        }
    }
