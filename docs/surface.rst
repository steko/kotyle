.. _surface:

=====================================
 Calculating the surface of a vessel
=====================================

While certainly less used, it's quite easy to get the surface of a
solid of revolution using the Pappus's first centroid theorem.

Pappus's centroid theorem
=========================



.. math::

   A = s d

Why calculating surface ?
=========================

One might ask as well: why not ? A large number of different
quantification methods have been developed since the 1970s, but none
of them have taken into account the surface of the vessel.

It can be difficult to measure the planar surface of a potsherd, but
it doesn't mean that surface could not be adopted together with *EVE*,
or *MNV*.

How can I do that in practice ?
===============================

You could use a large number of small objects, to be temporarily
sticked on the surface covering it homogeneously. Then, you just
remove them and count / weight. This would work for sherds of small
size.

