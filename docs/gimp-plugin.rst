.. _gimp-plugin:

=======================================
 GNU Image Manipulation Program plugin
=======================================

If you use an image editor like the `GNU Image Manipulation Program`_
you can use Kotyle directly with a plugin to:

1. measure capacity
2. save the corresponding *vector path* of the vessel profile
   as GeoJSON data for further processing

The GNU Image Manipulation Program is a freely distributed program for image 
retouching and manipulation, available for all operating systems.

.. _`GNU Image Manipulation Program`: https://www.gimp.org/

Installation
============

Installing this plug-in is very easy and requires copying one file in the
right directory.

The file can be downloaded from https://codeberg.org/steko/kotyle/src/branch/main/gimp-plugin/vector-to-geojson.py

Open the :menuselection:`Edit --> Preferences` dialog from the main window of GIMP.

The dialog has a long list of various preferences. The last item in the list
should be "Folders" and among the sub-items there are "Plug-ins". When you
enter the preferences for plug-ins folders, you should find some paths listed
and you can open the path in your user directory directly from the same dialog.

Copy the ``vector-to-geojson.py`` file in that directory. On Linux or MacOS,
make sure the file is executable.

Restart The GIMP, and you will find the plug-in the
:menuselection:`Filters --> Kotyle` menu.

Before you start: check image resolution
========================================

It is very important that you check the image resolution is the
correct one, e.g. if you scanned a drawing at 300 dpi the image
file **must** be set at the same resolution, otherwise all
measurements will be wrong. This is fairly easy if you're working with
your own drawings, but it may require some work in case the
drawings are from older digital archives.

You can set the image resolution from the
:menuselection:`Image --> Print size...` menu item.

You will also need to make sure that the drawing is correcly rotated
with respect to the geometric lines, that is the rotation axis must be
a vertical straight line. Kotyle had a plugin for this but it became
obsolete with GIMP 2.10 that includes the same functionality by default
with the "Measure" tool and the associated "Straighten" function.

You can find the detailed manual on the GIMP website at
<https://docs.gimp.org/2.10/en/gimp-tool-measure.html>_.

Vector paths in GIMP
====================

Both plugins work with *Paths* in GIMP. Paths are separate from the
raster layers and can be drawn on top of the existing layers.

There is a specific panel for Paths where they can be removed and
renamed. Renaming is important here because we will need to be able to
pick one or more paths and use them with Kotyle.

You can find the most recent documentation about paths in the `GIMP
Manual`_.

.. _`GIMP Manual`: http://docs.gimp.org/2.10/en/gimp-using-paths.html

Drawing a vector path for a vessel profile
==========================================

The :guilabel:`Path` tool allows you to create vector paths. You
should either start from the vessel bottom (foot, base) or the top
(rim) taking care to draw along the **inner profile** (assuming you want
to calculate the volume of the vessel content).

Make sure you draw only one path for the profile. When you're done, go
the *Paths* panel in the main toolbox (close to the *Layers* panel)
and rename your path to something useful, like *Bowl Type 23*.

Measuring vessel capacity
=========================

Go to the :menuselection:`Plugins --> Kotyle` menu (it should be close
to the bottom of the menu) and click the *Measure vessel capacity* item.

A dialog will open, where you need to choose a few parameters:

- the path (directory) where you want to save the file
- the name of the file (default is ``profile.json`` but you want to
  choose a more sensible name if you have more than one profile)
  where to save the GeoJSON data of the vessel profile
- the vector path you want to export -- you will see that the
  drop-down list shows you the names of paths -- you can choose only
  one path
- title, description and author of the drawing, optional but useful

The computed capacity expressed in liters will be shown in a small popup window.

What to do with the GeoJSON?
============================

In short, you are going to have one ``.json`` file for each profile you
want to measure. These files are nothing special, you can open them
with any text editor and look at their contents.

More details about the GeoJSON format are found in :ref:`format`.

The *Measure vessel capacity* plugin will save your vessel profile in a
file (e.g. named ``profile.json``).

Kotyle has also a separate tool to calculate vessel volume from that file,
called :ref:`metro`. This tool is useful if you need to process many files
at once.
