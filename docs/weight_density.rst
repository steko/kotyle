.. _weight_density:

===============================================================
 Dealing with weight and density of ceramic vessels and sherds
===============================================================

Volume and capacity have a relationship with weight. This is
especially true for two separate ways of using such measurements:

- calculating the ratio between the weight of the container (either
  amphora or other container) and the weight of the liquid stored
  inside;
- using the weight of potsherds as a quantification method.

Suppose you have a complete profile of a vessel, in the form of a
drawing. As often happens, only part of the vessel is
preserved. Nevertheless, you would like to calculate what is the
proportion between the total vessel and the sherds that were found in
a specific archaeological context.

This is possible, and not difficult. The envisaged procedure should be
as follows:

1. calculate the total volume of the vessel from the drawing -- note
   that this means the volume of the ceramic body and not the capacity
   of the vessel
2. from the volume, obtain the total weight through density
3. weight your sherds and see the proportion

Knowing the density of a ceramic body might not be immediate, and it's
not certainly a good idea to stick with a standard value good for
contemporary ceramics (that is around 2 kg/dm³, by the way).

Measuring the density of a ceramic body
=======================================

The density and porosity of a ceramic body can be obtained through
Archimedes' principle. A formal procedure is described in [ASTM-C373]_
(not publicly available, unfortunately), and it needs only water and
a weight scale. Here follows a basic summary of the procedure.

.. [ASTM-C373] ASTM Standard C373 - 88(2006) "Standard Test Method for
	       Water Absorption, Bulk Density, Apparent Porosity and
	       Apparent Specific Gravity of Fired Whiteware Products",
	       ASTM International, West Conshohocken, PA, 2006. DOI:
	       10.1520/C0373-88R06, http://www.astm.org/

There are three measurements that need to be taken:
- dry weight :math:`W1`
- weight of the ceramic body in water :math:`W2`
- weight of the water-saturated ceramic body :math:`W3`

From these three values, the following properties can be obtained with
simple formulas:

 - external volume :math:`V = W3 – W2`
 - bulk density :math:`bD = W1/V`
 - apparent density :math:`aD = W1/(W1- W2)`
 - apparent porosity :math:`P = (W3 - W1)/V`
 - water absorption :math:`A = (W3 -W1)/W1`

.. note::

   This section was based on content published by Antonio Licciulli on
   his faculty website. You can find detailed content, mostly in
   Italian, at `this page
   <http://www.antonio.licciulli.unisalento.it/programmadelcorso.htm>`_

A basic example
---------------

What follows is a basic procedure that was tested on a limited number
of samples. You may want to use a spredsheet software to help with
the recording and the quick calculation of the properties derived from
the weight measurements.

You will need a container for water that is both wide enough to put
ceramic sherds inside it and tall enough to have them stay completely
below the water level. You will also need to keep the ceramic body
suspended in water, almost certainly in vertical position.

Step by step:

* weight the dry sherd (:math:`W1`);
* put the sherd inside the water, and let it stand on the bottom of
  the container until you see no air coming out of it: the ceramic
  body is now saturated with water;
* when the ceramic sherd is water-saturated, move the container with
  water (and the sherd on the bottom) on the weight scale and measure
  the weight :math:`p1`
* use a pair of pliers to lift the ceramic sherd from the bottom, and
  keep it suspended in water and completely below the water level --
  taking care that the pliers will stay as much as possible out of the
  water -- and measure weight :math:`p2`;
* take the ceramic sherd out of the water, let it quickly drip inside
  the container and weight the container with only water (the sherd
  should now be *out* of the container, on the desk), recording :math:`p3`
* optionally weight the ceramic sherd you just took out from water,
  recording :math:`p4`.

At this point, the *dry weight* is:

.. math::

    W1

while the weight of the ceramic body in water is:

.. math::

    W2 = p2 - p3

and the weight of the water-saturated ceramic body is:

.. math::

    W3 = p1 - p3 = p4

:math:`p1` *must* always be the largest measurement, and :math:`p3` the
      smallest one:

.. math::

    p1 > p2 > p3

All the required measurements are now available to calculate the
density, porosity and water absorption of the ceramic body.
