#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   GeoJSON Vector export - Export a vector as GeoJSON
#   Copyright 2020 Stefano Costa <steko@iosa.it>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.


import math, os, json

from itertools import compress, islice

from gimpfu import *

GEOJSON_TEMPLATE = { "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": [],
    },
  "properties": {
    "title": "",
    "description": "",
    "author": "",
    }
  }

class Point(object):
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __repr__(self):
        return "Point(x=%s, y=%s)" % (self.x, self.y)


class Polygon(object):
    '''Builds a Polygon object from an iterable of coordinate pairs.'''

    def __init__(self, points):
        self.coords = [(p[0], p[1]) for p in points ]

    @property
    def area(self):
        return self._shoelace_area()

    @property
    def centroid(self):
        return self._area_centroid()

    def _shoelace_area(self):
        '''The area of a polygon with Gauss's or shoelace formula.'''

        xs, ys = zip(*self.coords)

        yl = ys[1:] + ys[:1] # make 1st item last
        temp1 = sum(xy[0]*xy[1] for xy in zip(xs, yl))

        xl = xs[1:] + xs[:1] # make 1st item last
        temp2 = sum(xy[0]*xy[1] for xy in zip(xl, ys))

        return abs(temp1 - temp2) / 2.0

    def _area_centroid(self):
        '''The centroid of a polygon.'''

        xs, ys = zip(*self.coords)

        yl = ys[1:] + ys[:1] # make 1st item last
        xl = xs[1:] + xs[:1] # make 1st item last

        cx = 0
        for n, x in enumerate(xs):
            cx += (x + xl[n]) * (x * yl[n] - xl[n] * ys[n])
        cx = cx / (self.area * 6.0)

        cy = 0
        for n, y in enumerate(ys):
            cy += (y + yl[n]) * (y * xl[n] - yl[n] * xs[n])
        cy = cy / (self.area * 6.0)

        return Point(cx, cy)


class VesselCapacity(object):
    '''Compute the capacity of a ceramic vessel from its geometric
    profile.'''

    def __init__(self, coords):
        xs, ys = zip(*coords)

        # workaround mirrored profile
        xorder = xs[0] > xs[-1]
        yorder = ys[0] > ys[-1]

        # reduce coordinates to standard origin
        xmin = min(xs)
        ymin = min(ys)
        xmax = max(xs)

        if xorder and not yorder:
            xs = [xmax - x for x in xs]
        elif yorder and not xorder:
            xs = [xmax - x for x in xs]
        else:
            xs = [x - xmin for x in xs]
        ys = [y - ymin for y in ys]

        coords = zip(xs,ys)
        ymax = max(ys) # find missing corner
        coords.append((0.0, ymax))

        self.polygon = Polygon(coords)
        self.area = self.polygon.area
        self.v_centroid = self.polygon.centroid

    def surface(self):
        '''The curve surface of the vessel.'''

        s_distance = 2 * math.pi * self.s_centroid.x * self.arc_length

        # Pappus's theorem
        return self.arc_length * s_distance

    def volume(self):
        '''The capacity of the vessel.'''

        v_distance = 2 * math.pi * abs(self.v_centroid.x)

        # Pappus's theorem
        return self.area * v_distance

    def __str__(self):
        surface = 'Surface: %5.7f m²' % self.surface()
        volume = 'Capacity: %5.7f ℓ' % (self.volume())
        return '%s, %s' % (surface, volume)


def extract_points(pnts, xres, yres):
    '''Extracts point coordinates from GIMP vector.

    The format is undocumented, but it is a flat list of floats, 6 per
    vertex/node.

    For each node, there are three pairs of pixel coordinates (x and
    y) for the first anchor, the actual point and the second
    anchor. When the vector is a polygon, i.e.  there are no curve
    node, these 3 pairs are the same.

    Processing this format requires to split the list in blocks of
    six, then take the second pair in each block.

    Image resolution in dots per inch (DPI) is used to convert pixel
    coordinates in SI units, namely meters.

    The source coordinate system has a top-left origin (i.e. pixel
    0,0), like all screen-based image coordinate systems. Exporting to
    GeoJSON is a first step towards geometric processing, that usually
    happens in bottom-left (Cartesian) coordinate systems. This is the
    rationale for the change in coordinate systems.'''

    cpsel = [0,0,1,1,0,0]*(len(pnts)/6)
    xys = [i for i in compress(pnts, cpsel)]
    xs = xys[::2]
    ys = xys[1::2]

    # DPI get converted to meters
    xs = [ float(x) / xres * 127.0 / 5000.0 for x in xs ]
    ys = [ float(y) / yres * 127.0 / 5000.0 for y in ys ]

    # from top-left origin to bottom-left origin (Cartesian)
    ymax = max(ys)
    ys = [ ymax - y for y in ys ]
    xypoints = zip(xs,ys)
    return xypoints

def python_vector_to_geojson(timg, tdrawable, savepath, filename, vector, title, description, author):
    img = timg
    active_vectors = vector
    strk = active_vectors.strokes[0]
    pnts = strk.points[0]
    # DPI are vital to get real measures rather than pixels
    xres, yres = gimp.pdb.gimp_image_get_resolution(img)
    xypoints = extract_points(pnts, xres, yres)

    geojson = GEOJSON_TEMPLATE.copy()
    geojson["geometry"]["coordinates"] = xypoints
    geojson["properties"]["title"] = title
    geojson["properties"]["description"] = description
    geojson["properties"]["author"] = author

    def check_path(path):
        '''Copyright (C) 2003, 2005  Manish Singh <yosh@gimp.org>

        From GIMP_SOURCE/plug-ins/pygimp/plug-ins/py-slice.py'''

        path = os.path.abspath(path)
        if not os.path.exists(path):
            os.mkdir(path)
        return path

    savepath = check_path(savepath)

    if not os.path.isdir(savepath):
        savepath = os.path.dirname(savepath)

    filepath = os.path.join(savepath, filename)

    with open(filepath, 'wb') as export: # FIXME check for overwriting
        export.write(json.dumps(geojson, indent=4))

    vessel = VesselCapacity(xypoints)
    d =  {'volume': vessel.volume()*1000,
          'title':  gimp.pdb.gimp_item_get_name(tdrawable),
          }
    gimp.pdb.gimp_message('The capacity of "{title}" is {volume:03.2f} ℓ'.format(**d))

register(
        "python_fu_vector_to_geojson",
        "Measure vessel capacity",
        " - Measure vessel capacity",
        "Stefano Costa",
        "Stefano Costa",
        "2015",
        "<Image>/Filters/Kotyle/_Measure vessel capacity...",
        "RGB*, GRAY*",
        [
                (PF_DIRNAME, "savepath",  "Path for JSON export", os.getcwd()),
                (PF_STRING, "filename",  "Filename for export",  "profile.json"),
                (PF_VECTORS, "vector", "Vector to export", None),
                (PF_STRING, "title", "Title", ""),
                (PF_STRING, "description", "Description", ""),
                (PF_STRING, "author", "Author", ""),
        ],
        [],
        python_vector_to_geojson)

main()
