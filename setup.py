from setuptools import setup, find_packages
import sys, os

version = '0.0'

setup(name='Kotyle',
      version=version,
      description="Kotyle measures vessel capacity",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='ceramic capacity archaeology',
      author='Stefano Costa',
      author_email='steko@iosa.it',
      url='http://kotyle.iosa.it/',
      license='BSD',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
